﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class Banks
    {
        public int Id { get; set; }
        public string BankName { get; set; }
        public string SortCode { get; set; }
        public States BankBranch { get; set; }
        public int BankBranchId { get; set; }
        public string BankAddress { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
    }
}