﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace dms.Models
{
    public class UploadedDocuments
    {
        public int ID { get; set; }
        public string ReferenceNumber { get; set; }
        public string FilePath { get; set; }
        public string SystemGeneratedsReferenceNumber { get; set; }
    }
}