﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class StateAccounts
    {
        public int Id { get; set; }
        public Banks Bank { get; set; }
        public int BankId { get; set; }
        public string AccountNumber { get; set; }
        public string AccountCategory { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
    }
}