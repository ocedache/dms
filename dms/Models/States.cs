﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class States
    {
        public int Id { get; set; }
        public string StateName { get; set; }
    }
}