﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public enum AccountStatus
    {
        Activate = 1,
        Deactivate = 0
    }
}