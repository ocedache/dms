﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class MandateType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}