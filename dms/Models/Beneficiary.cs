﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class Beneficiary
    {
        public int Id { get; set; }
        public string BeneficiaryName { get; set; }
        public Banks BeneficiaryBank { get; set; }
        public int BeneficiaryBankId { get; set; }
        public string AccountNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string TinNo { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
    }
}