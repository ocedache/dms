﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class Voucher
    {
        public int Id { get; set; }
        public string DepartmentalNo { get; set; }
        public Beneficiary PaymentMadeTo { get; set; }
        public int PaymentMadeToId { get; set; }
        public DateTime VoucherDate { get; set; }
        public string PVNo { get; set; }
        public string AccountHead { get; set; }
        public string AccountSubHead { get; set; }
        public DateTime DateReceivedInTreasury { get; set; }
        public ApplicationUser PreparedBy { get; set; }
        public string PreparedById { get; set; }
        public ApplicationUser CheckedBy { get; set; }
        public string CheckedById { get; set; }
        public string OfficerControllingVote { get; set; }
        public ApplicationUser TreasuryCheckingOfficer { get; set; }
        public string TreasuryCheckingOfficerId { get; set; }
        public double TotalAmount { get; set; }
        public string AmountInWord { get; set; }
        public string VoucherDescription { get; set; }
        public double Rate { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
        public DocumentStatus Status { get; set; }
    }
}