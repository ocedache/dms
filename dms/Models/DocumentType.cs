﻿namespace dms.Models
{
    public class DocumentType
    {
        public int Id { get; set; }
        public string DocumentTypeName { get; set; }
    }
}