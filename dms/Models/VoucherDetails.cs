﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class VoucherDetails
    {
        public int Id { get; set; }
        public string PVNo { get; set; }
        public DateTime VoucherDate { get; set; }
        public string VoucherDescription { get; set; }
        public double Rate { get; set; }
        public double Amount { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
    }
}