﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class Documents
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DocumentType DocumentType { get; set; }
        public int DocumentTypeId { get; set; }
        public string Subject { get; set; }
        public string Note { get; set; }
        public UserGroup TransferedTo { get; set; }
        public int TransferedToId { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public DocumentStatus Status { get; set; }
        public string Address { get; set; }
        public string RefNo { get; set; }
    }
}