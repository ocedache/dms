﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace dms.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public DbSet<Documents> Documents { get; set; }
        public DbSet<DocumentType> DocumentType { get; set; }
        public DbSet<UserGroup> UserGroup { get; set; }
        public DbSet<Voucher> Voucher { get; set; }
        public DbSet<Mandate> Mandate { get; set; }
        public DbSet<MandateType> MandateType { get; set; }
        public DbSet<Beneficiary> Beneficiary { get; set; }
        public DbSet<Banks> Banks { get; set; }
        public DbSet<StateAccounts> StateAccounts { get; set; }
        public DbSet<States> States { get; set; }
        public DbSet<VoucherDetails> VoucherDetails { get; set; }

        public DbSet<UploadedDocuments> UploadedDocuments { get; set; }
        public DbSet<Files> Files { get; set; }
        public DbSet<FileWorkflow> FileWorkflow{ get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}