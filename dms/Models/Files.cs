﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class Files
    {
        public int Id { get; set; }
        public string ReferenceNumber { get; set; }
        public DocumentStatus Status { get; set; }
        public DateTime DateSubmitted { get; set; }
        public string SystemGeneratedReferenceNumber { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
    }
}