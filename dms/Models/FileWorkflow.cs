﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class FileWorkflow
    {
        public int Id { get; set; }
        public Files File { get; set; }
        public int FileId { get; set; }
        public ApplicationUser ForwardedFrom { get; set; }
        [Required]
        public string ForwardedFromId { get; set; }
        public DateTime DateForwarded { get; set; }
        public string SystemGeneratedReferenceNumber { get; set; }
        [Required]
        public string ActionTaken { get; set; }
        [Required]
        public string OriginatingOffice { get; set; }
    }
}