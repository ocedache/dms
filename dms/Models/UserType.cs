﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public enum UserType
    {
        Administrator = 1,
        Superior = 2,
        Staff = 3,
        User = 4
    }
}