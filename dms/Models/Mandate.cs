﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public class Mandate
    {
        public int Id { get; set; }
        public StateAccounts PayingAccount { get; set; }
        public int? PayingAccountId { get; set; }
        public MandateType MandateType { get; set; }
        public int? MandateTypeId { get; set; }
        public DateTime? MandateDate { get; set; }
        public string PVNo { get; set; }
        public double Amount { get; set; }
        public string PurposeOfPayment { get; set; }
        public string RefNo { get; set; }
        public string CodeNo { get; set; }
        public string MandateNote { get; set; }
        public DocumentStatus Status { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
    }
}