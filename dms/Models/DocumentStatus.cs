﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dms.Models
{
    public enum DocumentStatus
    {
        Pending = 1,
        Sent = 2,
        Paid = 3,
        CheckedForMandate = 4,
        MandateProcessed = 5,
        AwaitingPayment = 6,
        Commissioner = 7,
        PermanentSecretary = 8,
        AccountantGeneral = 9,
        DeputyAccountGeneral = 10,
        DeputyDirectorTreasury = 11,
        DirectorFunds = 12,
        Bank = 13,
        DiscardVoucher = 14
    }
}