﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using dms.Models;
using Microsoft.AspNet.Identity;

namespace dms.Controllers
{
    public class WorkflowController : Controller
    {
        ApplicationDbContext _context;
        public WorkflowController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Workflow
        public ActionResult Index()
        {
            return View();
        }
        //FORWARD applications in the workflow and change document file status to reflect it
        public ActionResult Forward(string fileId, string systemReferenceGeneratedReferenceNumber)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();

            int file = int.Parse(fileId);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            if (roles.Single().Value.ToString() == "Commissioner")
            {
                //Insert workflow entry
                var workflowEntry = new FileWorkflow
                {
                    FileId = file,
                    ForwardedFromId = userId,
                    DateForwarded = DateTime.Now,
                    SystemGeneratedReferenceNumber = systemReferenceGeneratedReferenceNumber,
                    ActionTaken = "Forwarded File to Permananent Secretary",
                    OriginatingOffice = "Office of the Honorable Commissioner for Finance"
                };

                _context.FileWorkflow.Add(workflowEntry);
                int result = _context.SaveChanges();

                //Update file status
                var fileToUpdate = _context.Files.Where(f => f.Id == file).FirstOrDefault();

                fileToUpdate.Status = DocumentStatus.PermanentSecretary;

                result = _context.SaveChanges();
            }
            else if (roles.Single().Value.ToString() == "Permanent Secretary")
            {
                //Insert workflow entry
                var workflowEntry = new FileWorkflow
                {
                    FileId = file,
                    ForwardedFromId = userId,
                    DateForwarded = DateTime.Now,
                    SystemGeneratedReferenceNumber = systemReferenceGeneratedReferenceNumber,
                    ActionTaken = "Forwarded File to Accountant General",
                    OriginatingOffice = "Permanent Secretary"
                };

                _context.FileWorkflow.Add(workflowEntry);
                int result = _context.SaveChanges();

                //Update file status
                var fileToUpdate = _context.Files.Where(f => f.Id == file).FirstOrDefault();

                fileToUpdate.Status = DocumentStatus.AccountantGeneral;

                result = _context.SaveChanges();
            }
            else if (roles.Single().Value.ToString() == "Accountant General")
            {
                //Insert workflow entry
                var workflowEntry = new FileWorkflow
                {
                    FileId = file,
                    ForwardedFromId = userId,
                    DateForwarded = DateTime.Now,
                    SystemGeneratedReferenceNumber = systemReferenceGeneratedReferenceNumber,
                    ActionTaken = "Forwarded File to Deputy Accountant General",
                    OriginatingOffice = "Accountant General"
                };

                _context.FileWorkflow.Add(workflowEntry);
                int result = _context.SaveChanges();

                //Update file status
                var fileToUpdate = _context.Files.Where(f => f.Id == file).FirstOrDefault();

                fileToUpdate.Status = DocumentStatus.DeputyAccountGeneral;

                result = _context.SaveChanges();
            }
            else if (roles.Single().Value.ToString() == "Deputy Accountant General")
            {
                //Insert workflow entry
                var workflowEntry = new FileWorkflow
                {
                    FileId = file,
                    ForwardedFromId = userId,
                    DateForwarded = DateTime.Now,
                    SystemGeneratedReferenceNumber = systemReferenceGeneratedReferenceNumber,
                    ActionTaken = "Forwarded File to Deputy Director Treasury",
                    OriginatingOffice = "Deputy Accountant General"
                };

                _context.FileWorkflow.Add(workflowEntry);
                int result = _context.SaveChanges();

                //Update file status
                var fileToUpdate = _context.Files.Where(f => f.Id == file).FirstOrDefault();

                fileToUpdate.Status = DocumentStatus.DeputyDirectorTreasury;

                result = _context.SaveChanges();
            }
            else if (roles.Single().Value.ToString() == "Deputy Director Treasury")
            {
                //Insert workflow entry
                var workflowEntry = new FileWorkflow
                {
                    FileId = file,
                    ForwardedFromId = userId,
                    DateForwarded = DateTime.Now,
                    SystemGeneratedReferenceNumber = systemReferenceGeneratedReferenceNumber,
                    ActionTaken = "Forwarded File to Director Funds",
                    OriginatingOffice = "Deputy Director Treasury"
                };

                _context.FileWorkflow.Add(workflowEntry);
                int result = _context.SaveChanges();

                //Update file status
                var fileToUpdate = _context.Files.Where(f => f.Id == file).FirstOrDefault();

                fileToUpdate.Status = DocumentStatus.DirectorFunds;

                result = _context.SaveChanges();
            }
            else if (roles.Single().Value.ToString() == "Director Funds")
            {
                //Insert workflow entry
                var workflowEntry = new FileWorkflow
                {
                    FileId = file,
                    ForwardedFromId = userId,
                    DateForwarded = DateTime.Now,
                    SystemGeneratedReferenceNumber = systemReferenceGeneratedReferenceNumber,
                    ActionTaken = "Forwarded Mandate to Paying Banks",
                    OriginatingOffice = "Director Funds"
                };

                _context.FileWorkflow.Add(workflowEntry);
                int result = _context.SaveChanges();

                //Update file status
                var fileToUpdate = _context.Files.Where(f => f.Id == file).FirstOrDefault();

                fileToUpdate.Status = DocumentStatus.Bank;

                result = _context.SaveChanges();
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}