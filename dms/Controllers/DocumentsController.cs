﻿using dms.Models;
using dms.View_Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using dms.HelperClasses;
using System;

namespace dms.Controllers
{
    public class DocumentsController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public DocumentsController()
        {
            _context = new ApplicationDbContext();

        }

        public ActionResult Keepalive()
        {
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult NewDocument(int? Id)
        {
            if (Id != null)
            {
                var Documents = _context.Documents.Where(d => d.Id == Id).FirstOrDefault();

                var viewModel = new DocumentViewModel
                {
                    Date = Documents.Date,
                    TypeId = Documents.DocumentTypeId,
                    Subject = Documents.Subject,
                    Note = Documents.Note,
                    Address = Documents.Address,
                    RefNo = Documents.RefNo
                };
                viewModel.Id = Id;
                viewModel.DocumentType = _context.DocumentType.ToList();
                viewModel.UserGroup = _context.UserGroup.ToList();
                var userId = User.Identity.GetUserId();
                viewModel.user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

                return View("NewDocument", viewModel);
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var documentType = _context.DocumentType.ToList();
                return View(new DocumentViewModel
                {
                    user = user,
                    role = roles,
                    DocumentType = documentType
                });
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewDocument(DocumentViewModel model)
        {
            if (model.Id != null)
            {
                var Document = _context.Documents.Where(a => a.Id == model.Id).FirstOrDefault();

                Document.Date = model.Date;
                Document.DocumentTypeId = model.TypeId;
                Document.Subject = model.Subject;
                Document.Note = model.Note;
                Document.Address = model.Address;
                Document.RefNo = model.RefNo;
                _context.SaveChanges();

                return RedirectToAction("Documents", "Documents");
            }
            else
            {
                var userId = User.Identity.GetUserId();
                model.user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var roleClaimType = userIdentity.RoleClaimType;
                model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

                //Save data to db
                if (ModelState.IsValid)
                {
                    //Save data to db
                    var Document = new Documents
                    {
                        Date = model.Date,
                        DocumentTypeId = model.TypeId,
                        Subject = model.Subject,
                        Note = model.Note,
                        UserId = userId,
                        Status = DocumentStatus.Commissioner,
                        Address = model.Address,
                        RefNo = model.RefNo,
                        TransferedToId = 5
                    };
                    _context.Documents.Add(Document);
                    _context.SaveChanges();
                    return RedirectToAction("Documents", "Documents");
                }
            }

            return View(model);
        }
        public ActionResult AuthorizeDocument(int? Id)
        {
            var Documents = _context.Documents.Where(d => d.Id == Id).FirstOrDefault();

            var viewModel = new DocumentViewModel
            {
                Date = Documents.Date,
                TypeId = Documents.DocumentTypeId,
                Subject = Documents.Subject,
                Note = Documents.Note,
                Address = Documents.Address,
                RefNo = Documents.RefNo
            };
            viewModel.Id = Id;
            viewModel.DocumentType = _context.DocumentType.ToList();
            viewModel.UserGroup = _context.UserGroup.ToList();
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            viewModel.TransferedTo = _context.UserGroup.ToList();

            return View("AuthorizeDocument", viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AuthorizeDocument(DocumentViewModel model)
        {
            var Document = _context.Documents.Where(a => a.Id == model.Id).FirstOrDefault();

            Document.Date = model.Date;
            Document.DocumentTypeId = model.TypeId;
            Document.Subject = model.Subject;
            Document.Note = model.Note;
            Document.Address = model.Address;
            Document.RefNo = model.RefNo;
            Document.TransferedToId = model.TransferedToId;
            Document.Status = DocumentStatus.Commissioner;
            _context.SaveChanges();

            return RedirectToAction("Documents", "Documents");
        }

        public ActionResult Documents()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DocumentViewModel
            {
                user = user,
                role = roles
            });
        }
        public string GetDocuments(string sEcho, int iDisplayStart, int iDisplayLength, string sSearch)
        {
            //Get User Id
            var userId = User.Identity.GetUserId();
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            string test = string.Empty;
            sSearch = sSearch.ToLower();
            int totalRecord = _context.Documents.Where(d => d.User.UserGroupId == user.UserGroupId).Count();
            var documents = new List<Documents>();
            if (!string.IsNullOrEmpty(sSearch))
                documents = _context.Documents.Include(p => p.DocumentType).Include(p => p.TransferedTo).Include(p => p.User).Where(d => d.User.UserGroupId == user.UserGroupId).Where(p => p.Subject.Contains(sSearch)
                                                        || p.Address.Contains(sSearch)
                                                        || p.RefNo.Contains(sSearch)
                                                        || p.TransferedTo.GroupName.Contains(sSearch)
                                                        || p.DocumentType.DocumentTypeName.Contains(sSearch)
                                                        || p.User.FirstName.Contains(sSearch)
                                                        || p.User.LastName.Contains(sSearch)).OrderBy(a => a.Date).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            else
                documents = _context.Documents.Include(p => p.DocumentType).Include(p => p.TransferedTo).Include(p => p.User).Where(d => d.User.UserGroupId == user.UserGroupId).OrderByDescending(a => a.Date).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            var result = (from p in documents
                          select new
                          {
                              Date = p.Date.ToShortDateString(),
                              Time = p.Date.ToShortTimeString(),
                              Subject = p.Subject,
                              DocumentType = p.DocumentType.DocumentTypeName,
                              SentTo = p.TransferedTo.GroupName,
                              Status = p.Status,
                              Id = p.Id
                          }
                         ).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.Append("{");
            sb.Append("\"sEcho\": ");
            sb.Append(sEcho);
            sb.Append(",");
            sb.Append("\"iTotalRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"iTotalDisplayRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"aaData\": ");
            sb.Append(JsonConvert.SerializeObject(result));
            sb.Append("}");
            return sb.ToString();
        }
        public string GetFiles(string sEcho, int iDisplayStart, int iDisplayLength, string sSearch)
        {
            //Get User Id
            var userId = User.Identity.GetUserId();
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            string test = string.Empty;
            sSearch = sSearch.ToLower();
            int totalRecord;
            var files = new List<Files>();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Get the logged in user's role
            string userRole = roles.Single().Value.ToString();

            #region return record count for datatable based on the user's role
            //Return the total number of files for reporting based on the logged in user's role
            switch (userRole)
            {
                case "Commissioner":
                    totalRecord = _context.Files.Count();
                    break;
                case "Permanent Secretary":
                    totalRecord = _context.Files.Where(f => f.Status == DocumentStatus.PermanentSecretary).Count();
                    break;
                case "Accountant General":
                    totalRecord = _context.Files.Where(f => f.Status == DocumentStatus.AccountantGeneral).Count();
                    break;
                case "Deputy Accountant General":
                    totalRecord = _context.Files.Where(f => f.Status == DocumentStatus.DeputyAccountGeneral).Count();
                    break;
                case "Director Funds":
                    totalRecord = _context.Files.Where(f => f.Status == DocumentStatus.DirectorFunds).Count();
                    break;
                default:
                    totalRecord = 0;
                    break;
            }
            #endregion

            #region Report files based on user roles
            if (!string.IsNullOrEmpty(sSearch))
            {
                files = _context.Files.Where(file => file.ReferenceNumber.Contains(sSearch)).ToList();
            }
            else
            {
                if (roles.Single().Value.ToString() == "Commissioner")
                {
                    files = _context.Files
                        .OrderByDescending(file => file.DateSubmitted)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength)
                        .ToList();
                }
                else if (roles.Single().Value.ToString() == "Permanent Secretary")
                {
                    files = _context.Files.Where(f => f.Status == DocumentStatus.PermanentSecretary)
                        .OrderByDescending(file => file.DateSubmitted)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength)
                        .ToList();
                }
                else if (roles.Single().Value.ToString() == "Accountant General")
                {
                    files = _context.Files.Where(f => f.Status == DocumentStatus.AccountantGeneral)
                        .OrderByDescending(file => file.DateSubmitted)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength)
                        .ToList();
                }
                else if ((roles.Single().Value.ToString() == "Deputy Accountant General"))
                {
                    files = _context.Files.Where(f => f.Status == DocumentStatus.DeputyAccountGeneral)
                        .OrderByDescending(file => file.DateSubmitted)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength)
                        .ToList();
                }
                else if (roles.Single().Value.ToString() == "Deputy Director Treasury")
                {
                    files = _context.Files.Where(f => f.Status == DocumentStatus.DeputyDirectorTreasury)
                        .OrderByDescending(file => file.DateSubmitted)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength)
                        .ToList();
                }
                else if (roles.Single().Value.ToString() == "Director Funds")
                {
                    files = _context.Files.Where(f => f.Status == DocumentStatus.DirectorFunds)
                        .OrderByDescending(file => file.DateSubmitted)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength)
                        .ToList();
                }
                else
                {
                    files = _context.Files.OrderByDescending(file => file.DateSubmitted).Skip(iDisplayStart).Take(iDisplayLength).ToList();
                }
            }
            #endregion
            var result = (from p in files
                          select new
                          {
                              DateSubmitted = p.DateSubmitted.ToString("dddd, dd MMMM yyyy HH:mm:ss"),
                              ReferenceNumber = p.ReferenceNumber,
                              Status = p.Status,
                              Id = p.SystemGeneratedReferenceNumber
                          }
                         ).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.Append("{");
            sb.Append("\"sEcho\": ");
            sb.Append(sEcho);
            sb.Append(",");
            sb.Append("\"iTotalRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"iTotalDisplayRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"aaData\": ");
            sb.Append(JsonConvert.SerializeObject(result));
            sb.Append("}");
            return sb.ToString();
        }
        public ActionResult DocumentDetails(int? Id)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var DocuDetails = _context.Documents.Include(d => d.TransferedTo).Include(d => d.User).Where(d => d.Id == Id).FirstOrDefault();
            return View(new DocumentViewModel
            {
                user = user,
                role = roles,
                DocumentDetails = DocuDetails
            });
        }

        public ActionResult PreviewDocument(int? Id)
        {
            var stream = new MemoryStream();
            //Get Document
            var Doc = _context.Documents.Where(d => d.Id == Id).FirstOrDefault();
            //Previw Letter
            if (Doc.DocumentTypeId == 1)
            {
                //Generate Document Preview 
                Letter report = new Letter();
                report.Parameters["SELDOCID"].Value = Id;
                stream = new MemoryStream();
                report.ExportToPdf(stream);
            }
            //Preview Memo
            else if (Doc.DocumentTypeId == 2)
            {
                //Generate Document Preview 
                Memo report = new Memo();
                report.Parameters["SELDOCID"].Value = Id;
                stream = new MemoryStream();
                report.ExportToPdf(stream);
            }
            return File(stream.GetBuffer(), "application/pdf");
        }
        public ActionResult UploadDocuments()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new UploadedDocumentsViewModel
            {
                user = user,
                role = roles
            });
        }
        [HttpPost]
        public ActionResult UploadDocuments(UploadedDocumentsViewModel model)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                string systemGeneratedReferenceNumber;
                #region Check if reference number exists in DB
                string refNumber = model.ReferenceNumber;
                var referenceCheck = _context.UploadedDocuments.Where(doc => doc.ReferenceNumber == refNumber).FirstOrDefault();
                if (referenceCheck != null)
                {
                    //If the reference number 
                    systemGeneratedReferenceNumber = referenceCheck.SystemGeneratedsReferenceNumber;
                }
                else
                {
                    //Create Unique ID for every application
                    systemGeneratedReferenceNumber = ReferenceNumberGenerator.GenerateReferenceNumber();
                }
                #endregion

                //iterating through multiple file collection   
                foreach (HttpPostedFileBase file in model.FilePaths)
                {
                    #region upload scanned files to server
                    var extension = Path.GetExtension(file.FileName);

                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file.FileName);

                    string image = "/Files/Uploads/" + systemGeneratedReferenceNumber + "/" + fileNameWithExtension;
                    string path = System.IO.Path.Combine(Server.MapPath("~/Files/Uploads"), systemGeneratedReferenceNumber);

                    string imageUrl = System.IO.Path.Combine(Server.MapPath("~/Files/Uploads/" + systemGeneratedReferenceNumber), fileNameWithExtension);

                    var directory = Directory.CreateDirectory(path);

                    file.SaveAs(imageUrl);
                    #endregion
                    //Checking file is available to save.  
                    if (file != null)
                    {
                        var DocumentUpload = new UploadedDocuments
                        {
                            FilePath = image,
                            ReferenceNumber = model.ReferenceNumber,
                            SystemGeneratedsReferenceNumber = systemGeneratedReferenceNumber
                        };
                        _context.UploadedDocuments.Add(DocumentUpload);
                        var result = _context.SaveChanges();
                    }
                }
                //Return message to view notifying user that files have been uploaded successfully
                TempData["Success"] = "All files uploaded successfully";
                #region Create File and File Workflow Entries in DB
                //Create file entry
                var fileEntry = new Files
                {
                    ReferenceNumber = model.ReferenceNumber,
                    Status = DocumentStatus.Commissioner,
                    DateSubmitted = DateTime.Now,
                    SystemGeneratedReferenceNumber = systemGeneratedReferenceNumber,
                    UserId = user.Id
                };
                _context.Files.Add(fileEntry);
                //Create file workflow entry
                var fileWorkflow = new FileWorkflow
                {
                    FileId = fileEntry.Id,
                    ForwardedFromId = user.Id,
                    DateForwarded = DateTime.Now,
                    SystemGeneratedReferenceNumber = systemGeneratedReferenceNumber,
                    ActionTaken = "Scanned and Uploaded File",
                    OriginatingOffice = "PA to the Honorable Commissioner of Finance"
                };
                _context.FileWorkflow.Add(fileWorkflow);
                #endregion
                //Commit to database
                _context.SaveChanges();

                return View(new UploadedDocumentsViewModel
                {
                    user = user,
                    role = roles
                });
            }
        }
        public ActionResult FileDetails(string Id)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Get all documents with the specified System Generated Reference Number
            var uploadedDocuments = _context.UploadedDocuments.Where(docs => docs.SystemGeneratedsReferenceNumber == Id).ToList();
            string referenceNumber = uploadedDocuments.FirstOrDefault().ReferenceNumber;
            var file = _context.Files.Include(f => f.User).Where(f => f.SystemGeneratedReferenceNumber == Id).FirstOrDefault();
            return View(new FileDetailsViewModel
            {
                user = user,
                role = roles,
                UploadedDocuments = uploadedDocuments,
                ReferenceNumber = referenceNumber,
                File = file,
                FileId = file.Id
            });
        }
    }
}