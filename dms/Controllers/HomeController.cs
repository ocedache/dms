﻿using dms.Models;
using dms.View_Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Net;
using Microsoft.AspNet.Identity.EntityFramework;

namespace dms.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public HomeController()
        {
            _context = new ApplicationDbContext();

        }

        public ActionResult Keepalive()
        {
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            int pendingDocuments;

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            #region Count pending files based on the status of the files
            //Count total number of pending documents
            if (roles.Single().Value.ToString() == "Commissioner")
            {
               pendingDocuments = _context.Files.Where(file => file.Status == DocumentStatus.Commissioner).Count();
            }
            else if (roles.Single().Value.ToString() == "PA")
            {
                pendingDocuments = 0;
            }
            else if (roles.Single().Value.ToString() == "Permanent Secretary")
            {
                pendingDocuments = _context.Files.Where(file => file.Status == DocumentStatus.PermanentSecretary).Count();
            }
            else if (roles.Single().Value.ToString() == "Accountant General")
            {
                pendingDocuments = _context.Files.Where(file => file.Status == DocumentStatus.AccountantGeneral).Count();
            }
            else if (roles.Single().Value.ToString() == "Deputy Accountant General")
            {
                pendingDocuments = _context.Files.Where(file => file.Status == DocumentStatus.DeputyAccountGeneral).Count();
            }
            else if (roles.Single().Value.ToString() == "Deputy Director Treasury")
            {
                pendingDocuments = _context.Files.Where(file => file.Status == DocumentStatus.DeputyDirectorTreasury).Count();
            }
            else if (roles.Single().Value.ToString() == "Director Funds")
            {
                pendingDocuments = _context.Files.Where(file => file.Status == DocumentStatus.DirectorFunds).Count();
            }
            else
            {
                pendingDocuments = _context.Files.Count();
            }
            #endregion

            return View(new DashboardViewModel
            {
                user = user,
                role = roles,
                pendingDocuments = pendingDocuments
            });
        }

        public ActionResult ManageUser(string UserID)
        {
            if (UserID != null)
            {
                var ManUser = _context.Users.Where(d => d.Id == UserID).FirstOrDefault();

                var viewModel = new ManageUserViewModel
                {
                    FirstName = ManUser.FirstName,
                    LastName = ManUser.LastName,
                    Email = ManUser.Email,
                    GroupId = ManUser.UserGroupId
                };
                viewModel.UserID = UserID;
                viewModel.UserGroup = _context.UserGroup.ToList();
                viewModel.Users = _context.Users.Include(u => u.UserGroup).ToList();
                var userId = User.Identity.GetUserId();
                viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

                return View("ManageUser", viewModel);
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var userGroup = _context.UserGroup.ToList();
                var UserList = _context.Users.Include(u => u.UserGroup).ToList();

                return View(new ManageUserViewModel
                {
                    user = user,
                    role = roles,
                    Users = UserList,
                    UserGroup = userGroup
                });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ManageUser(ManageUserViewModel model)
        {
            if (model.UserID != null)
            {
                #region upload suspect photo file
                var extension = Path.GetExtension(model.FileName);
                string image = null;
                if (model.PassportUpload != null)
                {
                    var fileNameWithExtension = Path.GetFileName(model.PassportUpload.FileName);
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.PassportUpload.FileName);

                    image = "/images/profile/" + model.FirstName + "/" + fileNameWithExtension;
                    string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), model.FirstName);

                    string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + model.FirstName), fileNameWithExtension);

                    var directory = Directory.CreateDirectory(path);

                    model.PassportUpload.SaveAs(imageUrl);
                }
                #endregion

                var ManUser = _context.Users.Where(a => a.Id == model.UserID).FirstOrDefault();

                ManUser.FirstName = model.FirstName;
                ManUser.LastName = model.LastName;
                ManUser.Email = model.Email;
                ManUser.UserName = model.Email;
                ManUser.PhoneNumber = model.PhoneNumber;
                ManUser.UserGroupId = model.GroupId;
                ManUser.PhotoUrl = image;
                ManUser.UserType = model.UserType;
                if (model.Password != null)
                {
                    ManUser.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                }
                _context.SaveChanges();
                TempData["Success"] = "Updated Successfully!";
                RouteData.Values.Remove("UserID");
                return RedirectToAction("ManageUser", "Home", null);
            }
            else
            {
                var userEmail = _context.Users.Where(m => m.Email == model.Email).SingleOrDefault();
                if (userEmail == null)
                {
                    #region upload suspect photo file
                    var extension = Path.GetExtension(model.FileName);
                    string image = null;
                    if (model.PassportUpload != null)
                    {
                        var fileNameWithExtension = Path.GetFileName(model.PassportUpload.FileName);
                        var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.PassportUpload.FileName);

                        image = "/images/profile/" + model.FirstName + "/" + fileNameWithExtension;
                        string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), model.FirstName);

                        string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + model.FirstName), fileNameWithExtension);

                        var directory = Directory.CreateDirectory(path);

                        model.PassportUpload.SaveAs(imageUrl);
                    }
                    #endregion
                    //Save data to db
                    var User = new ApplicationUser
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Email = model.Email,
                        UserName = model.Email,
                        UserGroupId = model.GroupId,
                        PhotoUrl = image,
                        EmailConfirmed = true,
                        Status = AccountStatus.Deactivate,
                        UserType = model.UserType
                    };
                    var result = await UserManager.CreateAsync(User, model.Password);
                    if (result.Succeeded)
                    {
                        TempData["Success"] = "Added Successfully!";
                        return RedirectToAction("ManageUser", "Home");
                    }
                    AddErrors(result);
                    model.UserGroup = _context.UserGroup.ToList();
                    model.Users = _context.Users.ToList();
                    return View(model);
                }
                else
                {
                    //Add message to view
                    ModelState.AddModelError("", "Email address used for to create this account already exist for another user, please use a different email");
                    var userId = User.Identity.GetUserId();
                    model.user = _context.Users.Where(u => u.Id == userId).Single();
                    var userIdentity = (ClaimsIdentity)User.Identity;
                    var claims = userIdentity.Claims;
                    var roleClaimType = userIdentity.RoleClaimType;
                    model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                    model.UserGroup = _context.UserGroup.ToList();
                    model.Users = _context.Users.ToList();
                    
                    return View(model);
                }
            }
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public ActionResult ManageUserGroup(int? Id)
        {
            if (Id != null)
            {
                var Account = _context.UserGroup.Where(d => d.Id == Id).FirstOrDefault();

                var viewModel = new DashboardViewModel
                {
                    UserGroupName = Account.GroupName                    
                };

                var userId = User.Identity.GetUserId();
                viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                viewModel.UserGroupList = _context.UserGroup.ToList();
                
                return View("ManageUserGroup", viewModel);
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var UserGroupList = _context.UserGroup.ToList();

                return View(new DashboardViewModel
                {
                    user = user,
                    role = roles,
                    UserGroupList = UserGroupList                    
                });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageUserGroup(DashboardViewModel model)
        {
            if (model.Id != null)
            {
                var Account = _context.UserGroup.Where(a => a.Id == model.Id).FirstOrDefault();

                Account.GroupName = model.UserGroupName;
                
                _context.SaveChanges();
                TempData["Success"] = "Updated Successfully!";
                RouteData.Values.Remove("Id");
                return RedirectToAction("ManageUserGroup", "Home", null);
            }
            else
            {
                //Save data to db
                var Accounnt = new UserGroup
                {
                    GroupName = model.UserGroupName
                };
                _context.UserGroup.Add(Accounnt);
                _context.SaveChanges();
                return RedirectToAction("ManageUserGroup", "Home");
            }
        }

        public ActionResult ManageBeneficiaries(int? Id)
        {
            if (Id != null)
            {
                var Beneficiary = _context.Beneficiary.Where(d => d.Id == Id).FirstOrDefault();

                var viewModel = new BeneficiaryViewModel
                {
                    BeneficiaryName = Beneficiary.BeneficiaryName,
                    BeneficiaryBankId = Beneficiary.BeneficiaryBankId,
                    AccountNumber = Beneficiary.AccountNumber,
                    PhoneNumber = Beneficiary.PhoneNumber,
                    EmailAddress = Beneficiary.EmailAddress,
                    TinNo = Beneficiary.TinNo
                };
                
                var userId = User.Identity.GetUserId();
                viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                viewModel.BeneficiaryBank = _context.Banks.ToList();
                viewModel.BeneficiaryList = _context.Beneficiary.Include(i => i.BeneficiaryBank).ToList();

                return View("ManageBeneficiaries", viewModel);
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var BeneficiariesList = _context.Beneficiary.Include(u => u.BeneficiaryBank).ToList();
                var Banks = _context.Banks.ToList();

                return View(new BeneficiaryViewModel
                {
                    user = user,
                    role = roles,
                    BeneficiaryList = BeneficiariesList,
                    BeneficiaryBank = Banks
                });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageBeneficiaries(BeneficiaryViewModel model)
        {
            if (model.Id != null)
            {
                var Beneficiary = _context.Beneficiary.Where(a => a.Id == model.Id).FirstOrDefault();

                Beneficiary.BeneficiaryName = model.BeneficiaryName;
                Beneficiary.AccountNumber = model.AccountNumber;
                Beneficiary.BeneficiaryBankId = model.BeneficiaryBankId;
                Beneficiary.PhoneNumber = model.PhoneNumber;
                Beneficiary.EmailAddress = model.EmailAddress;
                Beneficiary.TinNo = model.TinNo;
                Beneficiary.UserId = model.UserId;
                Beneficiary.DateIn = DateTime.Now.Date;
                Beneficiary.TimeIn = DateTime.Now;
                
                _context.SaveChanges();
                TempData["Success"] = "Updated Successfully!";
                RouteData.Values.Remove("Id");
                return RedirectToAction("ManageBeneficiaries", "Home", null);
            }
            else
            {
                //Save data to db
                var Beneficiary = new Beneficiary
                {
                    BeneficiaryName = model.BeneficiaryName,
                    AccountNumber = model.AccountNumber,
                    BeneficiaryBankId = model.BeneficiaryBankId,
                    PhoneNumber = model.PhoneNumber,
                    EmailAddress = model.EmailAddress,
                    TinNo = model.TinNo,
                    UserId = User.Identity.GetUserId(),
                    DateIn = DateTime.Now.Date,
                    TimeIn = DateTime.Now
                };
                _context.Beneficiary.Add(Beneficiary);
                _context.SaveChanges();
                return RedirectToAction("ManageBeneficiaries", "Home");
            }
        }
        public ActionResult ManageBank(int? Id)
        {
            if (Id != null)
            {
                var Bank = _context.Banks.Where(d => d.Id == Id).FirstOrDefault();

                var viewModel = new BankViewModel
                {
                    BankName = Bank.BankName,
                    SortCode = Bank.SortCode,
                    BankAddress = Bank.BankAddress,
                    BankBranchId = Bank.BankBranchId
                };

                var userId = User.Identity.GetUserId();
                viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                viewModel.BankBranch = _context.States.ToList();
                viewModel.BankList = _context.Banks.Include(i => i.BankBranch).ToList();

                return View("ManageBank", viewModel);
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var BankList = _context.Banks.Include(u => u.BankBranch).ToList();
                var BankBranch = _context.States.ToList();

                return View(new BankViewModel
                {
                    user = user,
                    role = roles,
                    BankList = BankList,
                    BankBranch = BankBranch
                });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageBank(BankViewModel model)
        {
            if (model.Id != null)
            {
                var Bank = _context.Banks.Where(a => a.Id == model.Id).FirstOrDefault();

                Bank.BankName = model.BankName;
                Bank.BankBranchId = model.BankBranchId;
                Bank.BankAddress = model.BankAddress;
                Bank.SortCode = model.SortCode;
                Bank.UserId = User.Identity.GetUserId();
                Bank.DateIn = DateTime.Now.Date;
                Bank.TimeIn = DateTime.Now;

                _context.SaveChanges();
                TempData["Success"] = "Updated Successfully!";
                RouteData.Values.Remove("Id");
                return RedirectToAction("ManageBank", "Home", null);
            }
            else
            {
                //Save data to db
                var Bank = new Banks
                {
                    BankName = model.BankName,
                    BankBranchId = model.BankBranchId,
                    BankAddress = model.BankAddress,
                    SortCode = model.SortCode,
                    UserId = User.Identity.GetUserId(),
                    DateIn = DateTime.Now.Date,
                    TimeIn = DateTime.Now
                };
                _context.Banks.Add(Bank);
                _context.SaveChanges();
                return RedirectToAction("ManageBank", "Home");
            }
        }

        public ActionResult ManageStateAccounts(int? Id)
        {
            if (Id != null)
            {
                var Account = _context.StateAccounts.Where(d => d.Id == Id).FirstOrDefault();

                var viewModel = new StateAccountViewModel
                {
                    AccountNumber = Account.AccountNumber,
                    BankId = Account.BankId,
                    AccountCategory = Account.AccountCategory
                };

                var userId = User.Identity.GetUserId();
                viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                viewModel.Bank = _context.Banks.ToList();
                viewModel.AccountList = _context.StateAccounts.Include(i => i.Bank).ToList();
                viewModel.States = _context.States.ToList();


                return View("ManageStateAccounts", viewModel);
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var AccountList = _context.StateAccounts.Include(u => u.Bank).ToList();
                var Banks = _context.Banks.ToList();
                var states = _context.States.ToList();

                return View(new StateAccountViewModel
                {
                    user = user,
                    role = roles,
                    AccountList = AccountList,
                    Bank = Banks,
                    States = states
                });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageStateAccounts(StateAccountViewModel model)
        {
            if (model.Id != null)
            {
                var Account = _context.StateAccounts.Where(a => a.Id == model.Id).FirstOrDefault();

                Account.AccountNumber = model.AccountNumber;
                Account.BankId = model.BankId;
                Account.UserId = User.Identity.GetUserId();
                Account.AccountCategory = model.AccountCategory;
                Account.DateIn = DateTime.Now.Date;
                Account.TimeIn = DateTime.Now;

                _context.SaveChanges();
                TempData["Success"] = "Updated Successfully!";
                RouteData.Values.Remove("Id");
                return RedirectToAction("ManageStateAccounts", "Home", null);
            }
            else
            {
                //Save data to db
                var Accounnt = new StateAccounts
                {
                    AccountNumber = model.AccountNumber,
                    BankId = model.BankId,
                    AccountCategory = model.AccountCategory,
                    UserId = User.Identity.GetUserId(),
                    DateIn = DateTime.Now.Date,
                    TimeIn = DateTime.Now
                };
                _context.StateAccounts.Add(Accounnt);
                _context.SaveChanges();
                return RedirectToAction("ManageStateAccounts", "Home");
            }
        }

        public ActionResult ActivateUser(string UserID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            if (UserID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var UserRoleManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));

            var userToUpdate = _context.Users.Find(UserID);
            userToUpdate.Status = AccountStatus.Activate;
            userToUpdate.EmailConfirmed = true;

            //Put Desk definition here
            int userDesk = userToUpdate.UserGroupId;
            if (userDesk == 1)
            {
                UserRoleManager.AddToRole(userToUpdate.Id, "Permanent Secretary");
                var result = _context.SaveChanges();
            }
            else if (userDesk == 2)
            {
                UserRoleManager.AddToRole(userToUpdate.Id, "Accountant General");
                var result = _context.SaveChanges();
            }
            else if (userDesk == 3)
            {
                UserRoleManager.AddToRole(userToUpdate.Id, "Commissioner");
                var result = _context.SaveChanges();
            }
            else if (userDesk == 4)
            {
                UserRoleManager.AddToRole(userToUpdate.Id, "Deputy Accountant General");
                var result = _context.SaveChanges();
            }
            else if (userDesk == 5)
            {
                UserRoleManager.AddToRole(userToUpdate.Id, "PA");
                var result = _context.SaveChanges();
            }
            else if (userDesk == 6)
            {
                UserRoleManager.AddToRole(userToUpdate.Id, "Deputy Director Treasury");
                var result = _context.SaveChanges();
            }
            else if (userDesk == 7)
            {
                UserRoleManager.AddToRole(userToUpdate.Id, "Director Funds");
                var result = _context.SaveChanges();
            }
            
            _context.SaveChanges();
            return RedirectToAction("ManageUser");
        }
        public ActionResult DeactivateUser(string UserID)
        {
            if (UserID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userToUpdate = _context.Users.Find(UserID);
            userToUpdate.Status = AccountStatus.Deactivate;

            _context.SaveChanges();
            return RedirectToAction("ManageUser");
        }
        public ActionResult UserDetails(string UserID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var userDetails = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == UserID).Single();

            return View(new DashboardViewModel
            {
                user = user,
                role = roles,
                UserDetails = userDetails
            });
        }
    }
}