﻿using dms.Models;
using dms.View_Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.IO;

namespace dms.Controllers
{
    public class TimelineController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public TimelineController()
        {
            _context = new ApplicationDbContext();

        }

        public ActionResult Keepalive()
        {
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Timeline
        public ActionResult Index(int FileId)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Get the file to be used for the timeline
            List<FileWorkflow> fileWorkflow = _context.FileWorkflow
                                                .Include(f => f.ForwardedFrom)
                                                .Include(f => f.File)
                                                .Where(f => f.FileId == FileId).ToList();

            string systemGeneratedReferenceNumber = fileWorkflow.FirstOrDefault().SystemGeneratedReferenceNumber;

            //Path of first scanned page to be displayed in the timeline
            var filePath = _context.UploadedDocuments.Where(u => u.SystemGeneratedsReferenceNumber == systemGeneratedReferenceNumber).FirstOrDefault().FilePath;

            //Extension of the file uploaded
            string extension = Path.GetExtension(filePath);

            return View(new TimelineViewModel
            {
                user = user,
                role = roles,
                fileWorkflow = fileWorkflow,
                filePath = filePath,
                extension = extension
            });
        }
        // GET: Search
        public ActionResult Search()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }
        public ActionResult Document()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }
    }
}