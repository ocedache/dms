﻿using dms.Models;
using dms.View_Model;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace dms.Controllers
{
    //ONLY VIEWABLE BY THE COMMISSIONER OF FINANCE
    [Authorize(Roles = "Commissioner")]
    public class BankAccountsController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        public BankAccountsController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: BankAccounts
        public ActionResult AllAccounts()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            return View(new BankAccountsViewModel
            {
                user = user,
                role = roles
            });
        }
    }
}