﻿using dms.Models;
using dms.View_Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;
using dms.Reports;
using System.IO;

namespace dms.Controllers
{
    public class VoucherController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        string conString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public VoucherController()
        {
            _context = new ApplicationDbContext();

        }

        public ActionResult Keepalive()
        {
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult NewVoucher()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var beneficiaryList = _context.Beneficiary.ToList();

            #region Generate Voucher No 
            Voucher PV = new Voucher();
            //sort the Voucher and get the last insert employee.
            var lastVoucher = _context.Voucher.OrderByDescending(c => c.PVNo).FirstOrDefault();
            if (lastVoucher == null)
            {
                PV.PVNo = "REX00001";
            }
            else
            {
                //using string substring method to get the number of the last inserted employee's EmployeeID 
                PV.PVNo = "REX" + (Convert.ToInt32(lastVoucher.PVNo.Substring(5, lastVoucher.PVNo.Length - 5)) + 1).ToString("D5");
            }
            #endregion

            return View(new NewVoucherViewModel
            {
                user = user,
                role = roles,
                PaymentMadeTo = beneficiaryList,
                PVNo = PV.PVNo
            });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewVoucher(NewVoucherViewModel model)
        {
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Save data to db
            var Voucher = new Voucher
            {
                VoucherDate = model.DateAdded,
                PVNo = model.PVNo,
                DepartmentalNo = model.DepartmentalNo,
                AccountHead = model.AccountHead,
                AccountSubHead = model.AccountSubHead,
                PaymentMadeToId = model.PaymentMadeToId,
                TotalAmount = model.TotalAmount,
                AmountInWord = model.AmountInWord,
                PreparedBy = model.user,
                Rate = model.Rate,
                VoucherDescription = model.VoucherDescription,
                UserId = userId,
                DateIn = DateTime.Now.Date,
                TimeIn = DateTime.Now,
                Status = DocumentStatus.Pending,
                DateReceivedInTreasury = DateTime.Now.Date
            };
            _context.Voucher.Add(Voucher);
            _context.SaveChanges();
            
            return RedirectToAction("VoucherList", "Voucher");
        }

        public JsonResult InsertValue(NewVoucherViewModel[] itemlist, string VoucherNo, DateTime VoucherDate)
        {
            //Save voucher details to database
            foreach (NewVoucherViewModel i in itemlist)
            {
                if (i.VoucherDescription != null && i.Rate != 0 && i.Amount != 0)
                {
                    //loop through the array and insert value into database.
                    var VoucherDetails = new VoucherDetails
                    {
                        PVNo = VoucherNo,
                        VoucherDate = VoucherDate,
                        VoucherDescription = i.VoucherDescription,
                        Rate = i.Rate,
                        Amount = i.Amount,
                        DateIn = DateTime.Now.Date,
                        UserId = User.Identity.GetUserId(),
                        TimeIn = DateTime.Now
                    };
                    _context.VoucherDetails.Add(VoucherDetails);
                    _context.SaveChanges();
                }
            }
            return Json("Ok");
        }

        public ActionResult VoucherList()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }
        public string GetVoucherList(string sEcho, int iDisplayStart, int iDisplayLength, string sSearch)
        {
            //Get User Id
            var userId = User.Identity.GetUserId();
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            string test = string.Empty;
            sSearch = sSearch.ToLower();
            int totalRecord = _context.Voucher.Where(d => d.User.UserGroupId == user.UserGroupId).Count();
            var vouchers = new List<Voucher>();
            if (!string.IsNullOrEmpty(sSearch))
                vouchers = _context.Voucher.Include(p => p.PaymentMadeTo).Include(p => p.User).Where(p => p.PreparedBy.FirstName.Contains(sSearch)
                                                        || p.PVNo.Contains(sSearch)
                                                        || p.DepartmentalNo.Contains(sSearch)
                                                        || p.AccountHead.Contains(sSearch)
                                                        || p.AccountSubHead.Contains(sSearch)
                                                        || p.PaymentMadeTo.BeneficiaryName.Contains(sSearch)
                                                        || p.CheckedBy.FirstName.Contains(sSearch)
                                                        || p.PreparedBy.LastName.Contains(sSearch)).OrderBy(a => a.VoucherDate).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            else
                vouchers = _context.Voucher.Include(p => p.PaymentMadeTo).Include(p => p.User).OrderByDescending(a => a.VoucherDate).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            var result = (from p in vouchers
                          select new
                          {
                              Date = p.VoucherDate.ToShortDateString(),
                              DepartmentalNo = p.DepartmentalNo,
                              PVNo = p.PVNo,
                              PaymentMadeTo = p.PaymentMadeTo.BeneficiaryName,
                              Head = p.AccountHead,
                              Subhead = p.AccountSubHead,
                              Amount = p.TotalAmount,
                              Status = p.Status,
                              Id = p.Id
                          }
                         ).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.Append("{");
            sb.Append("\"sEcho\": ");
            sb.Append(sEcho);
            sb.Append(",");
            sb.Append("\"iTotalRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"iTotalDisplayRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"aaData\": ");
            sb.Append(JsonConvert.SerializeObject(result));
            sb.Append("}");
            return sb.ToString();
        }
        [Authorize]
        public ActionResult PendingVouchers()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var pendingVouchers = _context.Voucher.Include(p => p.PaymentMadeTo).Include(p => p.User).Where(p => p.Status == DocumentStatus.Pending).OrderByDescending(a => a.VoucherDate).ToList();
            return View(new NewVoucherViewModel
            {
                user = user,
                role = roles,
                PendingVouchers = pendingVouchers
            });
        }
        //Update database with list of selected voucher as checked for mandate
        [Authorize]
        public ActionResult ProcessMandate(int[] voucherIDs)
        {
            if (voucherIDs != null)
            {
                foreach (int voucherid in voucherIDs)
                {
                    Voucher obj = _context.Voucher.Find(voucherid);
                    obj.Status = DocumentStatus.CheckedForMandate;
                    _context.SaveChanges();
                }
            }
            //return Json("Ok");
            return RedirectToAction("GenerateMandate");
        }

        public ActionResult PaidVouchers()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }
        public string GetPaidVouchers(string sEcho, int iDisplayStart, int iDisplayLength, string sSearch)
        {
            //Get User Id
            var userId = User.Identity.GetUserId();
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            string test = string.Empty;
            sSearch = sSearch.ToLower();
            int totalRecord = _context.Voucher.Count();
            var vouchers = new List<Voucher>();
            if (!string.IsNullOrEmpty(sSearch))
                vouchers = _context.Voucher.Include(p => p.PaymentMadeTo).Include(p => p.User).Where(p => p.Status == DocumentStatus.Paid).Where(p => p.PreparedBy.FirstName.Contains(sSearch)
                                                        || p.PVNo.Contains(sSearch)
                                                        || p.DepartmentalNo.Contains(sSearch)
                                                        || p.AccountHead.Contains(sSearch)
                                                        || p.AccountSubHead.Contains(sSearch)
                                                        || p.PaymentMadeTo.BeneficiaryName.Contains(sSearch)
                                                        || p.CheckedBy.FirstName.Contains(sSearch)
                                                        || p.PreparedBy.LastName.Contains(sSearch)).OrderBy(a => a.VoucherDate).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            else
                vouchers = _context.Voucher.Include(p => p.PaymentMadeTo).Include(p => p.User).Where(p => p.Status == DocumentStatus.Paid).OrderByDescending(a => a.VoucherDate).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            var result = (from p in vouchers
                          select new
                          {
                              Date = p.VoucherDate.ToShortDateString(),
                              DepartmentalNo = p.DepartmentalNo,
                              PVNo = p.PVNo,
                              PaymentMadeTo = p.PaymentMadeTo.BeneficiaryName,
                              Head = p.AccountHead,
                              Subhead = p.AccountSubHead,
                              Amount = p.TotalAmount,
                              Status = p.Status,
                              Id = p.Id
                          }
                         ).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.Append("{");
            sb.Append("\"sEcho\": ");
            sb.Append(sEcho);
            sb.Append(",");
            sb.Append("\"iTotalRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"iTotalDisplayRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"aaData\": ");
            sb.Append(JsonConvert.SerializeObject(result));
            sb.Append("}");
            return sb.ToString();
        }

        public ActionResult VouchersAwaitingPayment()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var awaitingMandates = _context.Mandate.Include(m => m.MandateType).Include(m => m.PayingAccount).Where(aw => aw.Status == DocumentStatus.AwaitingPayment).ToList();
            var beneficiary = _context.Beneficiary.Include(b => b.BeneficiaryBank).ToList();
            var stateAccounts = _context.StateAccounts.ToList();
            var bankBranch = _context.States.ToList();
            var vouchers = _context.Voucher.ToList();

            return View(new MandateViewModel
            {
                user = user,
                role = roles,
                MandateAwaitingPayment = awaitingMandates,
                Beneficiaries = beneficiary,
                States = bankBranch,
                AllVouchers = vouchers
            });
        }
        public ActionResult VoucherDetails(int Id)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var voucherDetails = _context.Voucher.Include(v => v.PaymentMadeTo).Include(v => v.PreparedBy).Include(v => v.TreasuryCheckingOfficer).Include(v => v.CheckedBy).Where(v => v.Id == Id).FirstOrDefault();
            return View(new NewVoucherViewModel
            {
                user = user,
                role = roles,
                VoucherDetail = voucherDetails
            });
        }
        public ActionResult GenerateMandate()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var checkedVouchers = _context.Voucher.Include(p => p.PaymentMadeTo).Where(v => v.Status == DocumentStatus.CheckedForMandate).ToList();
            var beneficiary = _context.Beneficiary.Include(b => b.BeneficiaryBank).ToList();
            var stateAccounts = _context.StateAccounts.ToList();
            var bankBranch = _context.States.ToList();
            var mandateTypes = _context.MandateType.ToList();

            return View(new MandateViewModel
            {
                user = user,
                role = roles,
                StateAccounts = stateAccounts,
                CheckedVouchers = checkedVouchers,
                Beneficiaries = beneficiary,
                States = bankBranch,
                MandateType = mandateTypes
            });
        }
        [HttpPost]
        public ActionResult GenerateMandate(SaveMandateViewModel[] itemlist, int? PayingAccount, int? MandateType, string RefNo, string CodeNo, DateTime? manDate, string BankNote)
        {
            if (itemlist != null)
            {
                //Save voucher details to database
                foreach (SaveMandateViewModel i in itemlist)
                {
                    //loop through the array and insert value into database.
                    var Mandate = new Mandate
                    {
                        PayingAccountId = PayingAccount,
                        MandateTypeId = MandateType,
                        RefNo = RefNo,
                        CodeNo = CodeNo,
                        MandateDate = manDate,
                        MandateNote = BankNote,
                        PVNo = i.PVNo,
                        PurposeOfPayment = i.PurposeOfPayment,
                        Amount = i.Amount,
                        Status = DocumentStatus.AwaitingPayment,
                        DateIn = DateTime.Now.Date,
                        UserId = User.Identity.GetUserId(),
                        TimeIn = DateTime.Now
                    };
                    _context.Mandate.Add(Mandate);
                    var result = _context.SaveChanges();

                    //Update voucher table if save is successful
                    if (result > 0)
                    {
                        //Update status of voucher in voucher table
                        var voucher = _context.Voucher.Where(v => v.PVNo == i.PVNo).FirstOrDefault();
                        voucher.Status = DocumentStatus.AwaitingPayment;
                        voucher.UserId = User.Identity.GetUserId();
                        voucher.DateIn = DateTime.Now.Date;
                        voucher.TimeIn = DateTime.Now;
                        _context.SaveChanges();                        
                    }
                }
            }
            ViewBag.RefNo = RefNo;
            return RedirectToAction("VouchersAwaitingPayment");
        }

        //Update database with list of selected Mandates as paid
        [Authorize]
        public ActionResult PayMandate(int[] mandateIDs)
        {
            if (mandateIDs != null)
            {
                foreach (int mandateid in mandateIDs)
                {
                    Mandate obj = _context.Mandate.Find(mandateid);
                    obj.Status = DocumentStatus.Paid;
                    obj.UserId = User.Identity.GetUserId();
                    obj.DateIn = DateTime.Now.Date;
                    obj.TimeIn = DateTime.Now;
                    var result = _context.SaveChanges();

                    //Update voucher table if save is successful
                    if (result > 0)
                    {
                        //Update status of voucher in voucher table
                        var voucher = _context.Voucher.Where(v => v.PVNo == obj.PVNo).FirstOrDefault();
                        voucher.Status = DocumentStatus.Paid;
                        voucher.UserId = User.Identity.GetUserId();
                        voucher.DateIn = DateTime.Now.Date;
                        voucher.TimeIn = DateTime.Now;
                        _context.SaveChanges();
                    }
                }
            }
            //return Json("Ok");
            return RedirectToAction("PaidMandates");
        }
        public ActionResult PaidMandates()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var PaidMandates = _context.Mandate.Where(p => p.Status == DocumentStatus.Paid).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }
        public JsonResult GetPaidMandates()
        {
            //Get User Id
            
            var mandates = _context.Mandate.Include(p => p.PayingAccount).Include(p => p.MandateType).Include(p => p.User).Where(p => p.Status == DocumentStatus.Paid).OrderByDescending(a => a.MandateDate).ToList();
            var result = (from p in mandates
                          join s in _context.Voucher on p.PVNo equals s.PVNo
                          join b in _context.Beneficiary on s.PaymentMadeToId equals b.Id
                          select new
                          {
                              Date = p.MandateDate.Value.ToShortDateString(),
                              RefNo = p.RefNo,
                              PVNo = p.PVNo,
                              Ben = b.BeneficiaryName,
                              TypeOfMandate = p.MandateType.TypeName,
                              PayAccount = p.PayingAccount.AccountCategory,
                              AccountNo = p.PayingAccount.AccountNumber,
                              Amount = p.Amount,
                              Purpose = p.PurposeOfPayment,
                              Status = p.Status,
                              Id = p.Id
                          }
                         ).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadVoucher(string PVNo)
        {
            var stream = new MemoryStream();
            //Generate PDF of Mandate
            GenerateVoucher report = new GenerateVoucher();
            report.Parameters["SELPVNo"].Value = PVNo;
            stream = new MemoryStream();
            report.ExportToPdf(stream);
            //var cd = new System.Net.Mime.ContentDisposition
            //{
            //    FileName = PVNo,
            //    Inline = false,
            //};
            //Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(stream.GetBuffer(), "application/pdf");
        }
        public ActionResult MandateDetails(int Id)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var mandateDetails = _context.Mandate.Include(v => v.MandateType).Include(v => v.PayingAccount).Include(v => v.PayingAccount.Bank).Include(v => v.User).Where(v => v.Id == Id).FirstOrDefault();
            var voucherList = _context.Mandate.Include(v => v.MandateType).Include(v => v.PayingAccount).Include(v => v.PayingAccount.Bank).Include(v => v.PayingAccount.Bank.BankBranch).Where(v => v.RefNo == mandateDetails.RefNo).ToList();
            var vouchers = _context.Voucher.Include(v => v.PaymentMadeTo).Where(v => v.PVNo == mandateDetails.PVNo).ToList();
            return View(new NewVoucherViewModel
            {
                user = user,
                role = roles,
                MandateDetails = mandateDetails,
                MandateVouchers = voucherList,
                Vouchers = vouchers
            });
        }
        public ActionResult DownloadMandate(string RefNo)
        {
            var stream = new MemoryStream();
            //Generate PDF of Mandate
            GenerateMandates report = new GenerateMandates();
            report.Parameters["SELREFNO"].Value = RefNo;
            stream = new MemoryStream();
            report.ExportToPdf(stream);
            //var cd = new System.Net.Mime.ContentDisposition
            //{
            //    FileName = PVNo,
            //    Inline = false,
            //};
            //Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(stream.GetBuffer(), "application/pdf");
        }
        public ActionResult DiscardVoucher(int? Id)
        {
            var voucher = _context.Voucher.Find(Id);
            voucher.Status = DocumentStatus.DiscardVoucher;
            voucher.UserId = User.Identity.GetUserId();
            voucher.DateIn = DateTime.Now.Date;
            voucher.TimeIn = DateTime.Now;
            _context.SaveChanges();
            return RedirectToAction("DiscardedVouchers");
        }
        public ActionResult DiscardedVouchers()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }
        public string GetDiscardedVouchers(string sEcho, int iDisplayStart, int iDisplayLength, string sSearch)
        {
            //Get User Id
            var userId = User.Identity.GetUserId();
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            string test = string.Empty;
            sSearch = sSearch.ToLower();
            int totalRecord = _context.Voucher.Count();
            var vouchers = new List<Voucher>();
            if (!string.IsNullOrEmpty(sSearch))
                vouchers = _context.Voucher.Include(p => p.PaymentMadeTo).Include(p => p.User).Where(p => p.Status == DocumentStatus.DiscardVoucher).Where(p => p.PreparedBy.FirstName.Contains(sSearch)
                                                        || p.PVNo.Contains(sSearch)
                                                        || p.DepartmentalNo.Contains(sSearch)
                                                        || p.AccountHead.Contains(sSearch)
                                                        || p.AccountSubHead.Contains(sSearch)
                                                        || p.PaymentMadeTo.BeneficiaryName.Contains(sSearch)
                                                        || p.CheckedBy.FirstName.Contains(sSearch)
                                                        || p.PreparedBy.LastName.Contains(sSearch)).OrderBy(a => a.VoucherDate).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            else
                vouchers = _context.Voucher.Include(p => p.PaymentMadeTo).Include(p => p.User).Where(p => p.Status == DocumentStatus.DiscardVoucher).OrderByDescending(a => a.VoucherDate).Skip(iDisplayStart).Take(iDisplayLength).ToList();
            var result = (from p in vouchers
                          select new
                          {
                              Date = p.VoucherDate.ToShortDateString(),
                              DepartmentalNo = p.DepartmentalNo,
                              PVNo = p.PVNo,
                              PaymentMadeTo = p.PaymentMadeTo.BeneficiaryName,
                              Head = p.AccountHead,
                              Subhead = p.AccountSubHead,
                              Amount = p.TotalAmount,
                              Status = p.Status,
                              Id = p.Id
                          }
                         ).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.Append("{");
            sb.Append("\"sEcho\": ");
            sb.Append(sEcho);
            sb.Append(",");
            sb.Append("\"iTotalRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"iTotalDisplayRecords\": ");
            sb.Append(totalRecord);
            sb.Append(",");
            sb.Append("\"aaData\": ");
            sb.Append(JsonConvert.SerializeObject(result));
            sb.Append("}");
            return sb.ToString();
        }
        //Amount in Word
        [HttpPost]
        public JsonResult AmountInWord(decimal amount)
        {
            string inword = convertToWord.NumberToCurrencyText(amount);
            return Json(inword, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MandateList()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Include(u => u.UserGroup).Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }

        public JsonResult GetAllMandates()
        {
            //Get User Id

            var mandates = _context.Mandate.Include(p => p.PayingAccount).Include(p => p.MandateType).Include(p => p.User).OrderByDescending(a => a.MandateDate).ToList();
            var result = (from p in mandates
                          join s in _context.Voucher on p.PVNo equals s.PVNo
                          join b in _context.Beneficiary on s.PaymentMadeToId equals b.Id
                          select new
                          {
                              Date = p.MandateDate.Value.ToShortDateString(),
                              RefNo = p.RefNo,
                              PVNo = p.PVNo,
                              Ben = b.BeneficiaryName,
                              TypeOfMandate = p.MandateType.TypeName,
                              PayAccount = p.PayingAccount.AccountCategory,
                              AccountNo = p.PayingAccount.AccountNumber,
                              Amount = p.Amount,
                              Purpose = p.PurposeOfPayment,
                              Status = p.Status,
                              Id = p.Id
                          }
                         ).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}