﻿using Microsoft.Owin;
using Owin;
using dms.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

[assembly: OwinStartupAttribute(typeof(dms.Startup))]
namespace dms
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }

        // In this method we will create default User roles and Admin user for login    
        private void createRolesandUsers()
        {
            ApplicationDbContext _context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));


            // Create first Commissioner Role     
            if (!roleManager.RoleExists("Commissioner"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "Commissioner";
                roleManager.Create(role);
            }
            // Create PA Role     
            if (!roleManager.RoleExists("PA"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "PA";
                roleManager.Create(role);
            }
            // Create Permanent Secretary's Role     
            if (!roleManager.RoleExists("Permanent Secretary"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "Permanent Secretary";
                roleManager.Create(role);
            }
            // Create Accountant General's Role     
            if (!roleManager.RoleExists("Accountant General"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "Accountant General";
                roleManager.Create(role);
            }
            // Create Deputy Accountant General's Role     
            if (!roleManager.RoleExists("Deputy Accountant General"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "Deputy Accountant General";
                roleManager.Create(role);
            }
            // Create Deputy Director Treasury's Role     
            if (!roleManager.RoleExists("Deputy Director Treasury"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "Deputy Director Treasury";
                roleManager.Create(role);
            }
            // Create Director Funds Treasury's Role     
            if (!roleManager.RoleExists("Director Funds"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "Director Funds";
                roleManager.Create(role);
            }
            // Create Director Funds Treasury's Role     
            if (!roleManager.RoleExists("Administrator"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "Administrator";
                roleManager.Create(role);
            }
        }
    }
}
