﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using dms.View_Model;

namespace dms.View_Model
{
    public class UploadedDocumentsViewModel : BaseDashboardViewModel
    {
        public int ID { get; set; }
        public string ReferenceNumber { get; set; }
        public HttpPostedFileBase[] FilePaths { get; set; }
    }
}