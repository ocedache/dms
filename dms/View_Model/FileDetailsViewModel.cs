﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using dms.Models;

namespace dms.View_Model
{
    public class FileDetailsViewModel : BaseDashboardViewModel
    {
        public List<UploadedDocuments> UploadedDocuments { get; set; }
        public string ReferenceNumber { get; set; }
        public Files File { get; set; }
        public int FileId { get; set; }
    }
}