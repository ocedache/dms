﻿using dms.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace dms.View_Model
{
    public abstract class BaseDashboardViewModel
    {
        public ApplicationUser user { get; set; }
        public List<System.Security.Claims.Claim> role { get; set; }
        public string serverResponse { get; set; }
        public List<ApplicationUser> UserList { get; set; }
    }
    public class DashboardViewModel : BaseDashboardViewModel
    {
        public List<ApplicationUser> userList { get; set; }
        public int pendingDocuments { get; set; }
        //User Group
        public List<UserGroup> UserGroupList { get; set; }
        public string UserGroupName { get; set; }
        public int? Id { get; set; }
        public ApplicationUser UserDetails { get; set; }
    }
    public class TimelineViewModel : BaseDashboardViewModel
    {
        public List<FileWorkflow> fileWorkflow { get; set; }
        public string filePath { get; set; }
        public string extension { get; set; }
    }
    public class DocumentViewModel : BaseDashboardViewModel
    {
        public IEnumerable<UserGroup> TransferedTo { get; set; }
        public int TransferedToId { get; set; }
        public IEnumerable<DocumentType> DocumentType { get; set; }
        public int TypeId { get; set; }
        public DateTime Date { get; set; }
        public string Subject { get; set; }
        [AllowHtml]
        [Display(Name = "Note")]
        public string Note { get; set; }
        public string Address { get; set; }
        public string RefNo { get; set; }
        public int? Id { get; set; }
        public List<UserGroup> UserGroup { get; set; }
        public Documents DocumentDetails { get; set; }
        
    }
    public class ManageUserViewModel : BaseDashboardViewModel
    {
        public string UserID { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter email address.")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter your first name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please enter your last name")]
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public IEnumerable<UserGroup> UserGroup { get; set; }
        [Required(ErrorMessage = "Please select User Desk")]
        public int GroupId { get; set; }
        public HttpPostedFileBase PassportUpload { get; set; }
        public HttpPostedFileBase DefaultFace { get; set; }
        public HttpPostedFileBase DefaultImage { get; set; }
        public string FileName { get; set; }
        public List<ApplicationUser> Users { get; set; }
        public string Code { get; set; }
        public UserType UserType { get; set; }
    }
    public class NewVoucherViewModel : BaseDashboardViewModel
    {
        public string DepartmentalNo { get; set; }
        public IEnumerable<Beneficiary> PaymentMadeTo { get; set; }
        public int PaymentMadeToId { get; set; }
        public DateTime DateAdded { get; set; }
        public string PVNo { get; set; }
        public string AccountHead { get; set; }
        public string AccountSubHead { get; set; }
        public DateTime ReceivedInTresury { get; set; }
        public ApplicationUser CheckedBy { get; set; }
        public string CheckedById { get; set; }
        public string OfficerControllingVote { get; set; }
        public ApplicationUser TreasuryCheckingOfficer { get; set; }
        public string TreasuryCheckingOfficerId { get; set; }
        public double TotalAmount { get; set; }
        public string AmountInWord { get; set; }
        public string VoucherDescription { get; set; }
        public double Rate { get; set; }
        public double Amount { get; set; }
        //Voucher Tables
        public List<Voucher> PendingVouchers { get; set; }
        public Voucher VoucherDetail { get; set; }
        public Mandate MandateDetails { get; set; }
        public List<Mandate> MandateVouchers { get; set; }
        public List<Voucher> Vouchers { get; set; }
    }
    public class BeneficiaryViewModel : BaseDashboardViewModel
    {
        public int? Id { get; set; }
        public string BeneficiaryName { get; set; }
        public IEnumerable<Banks> BeneficiaryBank { get; set; }
        public int BeneficiaryBankId { get; set; }
        public string AccountNumber { get; set; }
        public string TinNo { get; set; }
        public string UserId { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
        public List<Beneficiary> BeneficiaryList { get; set; }
    }
    public class BankViewModel : BaseDashboardViewModel
    {
        public int? Id { get; set; }
        public string BankName { get; set; }
        public string SortCode { get; set; }
        public string BankAddress { get; set; }
        public IEnumerable<States> BankBranch { get; set; }
        public int BankBranchId { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
        public List<Banks> BankList { get; set; }
    }
    public class StateAccountViewModel : BaseDashboardViewModel
    {
        public int? Id { get; set; }
        public string AccountNumber { get; set; }
        public IEnumerable<Banks> Bank { get; set; }
        public int BankId { get; set; }
        public string AccountCategory { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime TimeIn { get; set; }
        public List<StateAccounts> AccountList { get; set; }
        public List<States> States { get; set; }
    }
    public class MandateViewModel : BaseDashboardViewModel
    {
        public IEnumerable<StateAccounts> StateAccounts { get; set; }
        public int StateAccountId { get; set; }
        public IEnumerable<MandateType> MandateType { get; set; }
        public int MandateTypeId { get; set; }
        public DateTime DateEntered { get; set; }
        public string PVNo { get; set; }
        public double Amount { get; set; }
        public string PurposeOfPayment { get; set; }
        public string RefNo { get; set; }
        public string CodeNo { get; set; }
        public string MandateNote { get; set; }
        public List<Voucher> CheckedVouchers { get; set; }
        public List<Beneficiary> Beneficiaries { get; set; }
        public List<States> States { get; set; }
        public List<Mandate> MandateAwaitingPayment { get; set; }
        public List<Voucher> AllVouchers { get; set; }
    }
    public class SaveMandateViewModel
    {
        public string PVNo { get; set; }
        public double Amount { get; set; }
        public string PurposeOfPayment { get; set; }
    }
}